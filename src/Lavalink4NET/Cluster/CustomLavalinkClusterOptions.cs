﻿using System;
using System.Linq;
using Lavalink4NET.Logging;
using Lavalink4NET.Player;

namespace Lavalink4NET.Cluster {
    /// <inheritdoc />
    public class CustomLavalinkClusterOptions<TNode> : LavalinkClusterOptions where TNode : LavalinkClusterNode {
        public CustomLavalinkClusterOptions(Func<LavalinkNodeOptions, IDiscordClientWrapper, ILogger?, ILavalinkCache?, TNode> nodeFactory) {
            NodeFactory = nodeFactory;
        }

        /// <summary>
        /// Factory for node creating
        /// </summary>
        public Func<LavalinkNodeOptions, IDiscordClientWrapper, ILogger?, ILavalinkCache?, TNode> NodeFactory { get; set; }
        
        public new CustomLoadBalancingStrategy<TNode> LoadBalacingStrategy { get; set; } = (cluster, nodes, _)
            => nodes.OrderByDescending(s => LoadBalancingStrategies.CalculateScore(s.Statistics)).First();
    }
}