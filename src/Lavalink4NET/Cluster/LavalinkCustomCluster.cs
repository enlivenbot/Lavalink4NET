﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Lavalink4NET.Events;
using Lavalink4NET.Logging;
using Lavalink4NET.Player;

namespace Lavalink4NET.Cluster {
    public class LavalinkCustomCluster<TNode> : LavalinkCluster where TNode : LavalinkClusterNode {
        protected new readonly CustomLoadBalancingStrategy<TNode> _loadBalacingStrategy;
        protected new readonly ObservableCollection<TNode> _nodes;
        private Func<LavalinkNodeOptions, IDiscordClientWrapper, ILogger?, ILavalinkCache?, TNode> _nodeFactory;

        public LavalinkCustomCluster(CustomLavalinkClusterOptions<TNode> options, IDiscordClientWrapper client, ILogger? logger = null,
                                     ILavalinkCache? cache = null) : 
            base(
            //Faking options to avoid breaking abstractions
            new LavalinkClusterOptions {StayOnline = options.StayOnline, LoadBalacingStrategy = LoadBalancingStrategies.LoadStrategy}, 
            client, logger, cache) {
            
            _loadBalacingStrategy = options.LoadBalacingStrategy;
            _nodeFactory = options.NodeFactory;
            _loadBalacingStrategy = options.LoadBalacingStrategy;
            _nodes = new ObservableCollection<TNode>(options.Nodes.Select(CreateNode));
            _nodes.CollectionChanged += (sender, args) => SyncNodes();
            SyncNodes();
        }

        private void SyncNodes() {
            base._nodes.Clear();
            base._nodes.AddRange(_nodes);
        }

        /// <summary>
        ///     Creates a new lavalink cluster node.
        /// </summary>
        /// <param name="nodeOptions">the node options</param>
        /// <returns>the created node</returns>
        protected new TNode CreateNode(LavalinkNodeOptions nodeOptions) {
            var lavalinkClusterNode = _nodeFactory(nodeOptions, _client, _logger, _cache);
            return lavalinkClusterNode;
        }

        public new IReadOnlyList<TNode> Nodes
        {
            get
            {
                lock (_nodesLock)
                {
                    return _nodes.ToArray();
                }
            }
        }
        
        public new TNode PreferredNode => GetPreferredNode();
        
        /// <summary>
        ///     Dynamically adds a node to the cluster asynchronously.
        /// </summary>
        /// <param name="nodeOptions">the node connection options</param>
        /// <returns>
        ///     a task that represents the asynchronous operation
        ///     <para>the cluster node info created for the node</para>
        /// </returns>
        /// <exception cref="ObjectDisposedException">thrown if the instance is disposed</exception>
        public new async Task<LavalinkClusterNode> AddNodeAsync(LavalinkNodeOptions nodeOptions)
        {
            EnsureNotDisposed();

            var node = CreateNode(nodeOptions);

            // initialize node
            await node.InitializeAsync();

            // add node info to nodes (so make it available for load balancing)
            lock (_nodesLock)
            {
                _nodes.Add(node);
            }

            return node;
        }
        
        /// <summary>
        ///     Gets the preferred node using the <see cref="LoadBalancingStrategy"/> specified in
        ///     the options.
        /// </summary>
        /// <param name="type">the type of the purpose for the node</param>
        /// <returns>the next preferred node</returns>
        /// <exception cref="InvalidOperationException">
        ///     thrown if the cluster has not been initialized.
        /// </exception>
        /// <exception cref="InvalidOperationException">thrown if no nodes is available</exception>
        /// <exception cref="ObjectDisposedException">thrown if the instance is disposed</exception>
        public new TNode GetPreferredNode(NodeRequestType type = NodeRequestType.Unspecified)
        {
            EnsureNotDisposed();

            if (!IsInitialized)
            {
                throw new InvalidOperationException("The cluster has not been initialized.");
            }

            lock (_nodesLock)
            {
                return GetPreferredNodeInternal(type);
            }
        }

        /// <summary>
        ///     Gets the node that serves the guild specified by <paramref name="guildId"/> (if no
        ///     node serves the guild, <see cref="PreferredNode"/> is used).
        /// </summary>
        /// <param name="guildId">the guild snowflake identifier</param>
        /// <param name="type">the type of the purpose for the node</param>
        /// <returns>the serving node for the specified <paramref name="guildId"/></returns>
        /// <exception cref="InvalidOperationException">
        ///     thrown if the cluster has not been initialized.
        /// </exception>
        /// <exception cref="ObjectDisposedException">thrown if the instance is disposed</exception>
        public new LavalinkNode GetServingNode(ulong guildId, NodeRequestType type = NodeRequestType.Unspecified)
        {
            EnsureNotDisposed();

            lock (_nodesLock)
            {
                var node = _nodes.FirstOrDefault(s => s.HasPlayer(guildId));

                if (node != null)
                {
                    return node;
                }

                return GetPreferredNodeInternal(type);
            }
        }
        
        private TNode GetPreferredNodeInternal(NodeRequestType type = NodeRequestType.Unspecified)
        {
            // find a connected node
            var nodes = _nodes.Where(s => s.IsConnected).ToArray();

            // no nodes available
            if (nodes.Length == 0)
            {
                throw new InvalidOperationException("No node available.");
            }

            // get the preferred node by the load balancing strategy
            var node = _loadBalacingStrategy(this, nodes, type);

            // update last usage
            node.LastUsage = DateTimeOffset.UtcNow;

            return node;
        }

        private async Task MovePlayerToNewNodeAsync(LavalinkNode sourceNode, LavalinkPlayer player)
        {
            if (player is null)
            {
                throw new ArgumentNullException(nameof(player));
            }

            if (!Nodes.Any(s => s.IsConnected))
            {
                _logger?.Log(this, $"(Stay-Online) No node available for player {player.GuildId}, dropping player...");

                // invoke event
                await OnPlayerMovedAsync(new PlayerMovedEventArgs(sourceNode, null, player));
                return;
            }

            // move node
            var targetNode = GetPreferredNode(NodeRequestType.Backup);
            await sourceNode.MovePlayerAsync(player, targetNode);

            // invoke event
            await OnPlayerMovedAsync(new PlayerMovedEventArgs(sourceNode, targetNode, player));
        }
    }
}