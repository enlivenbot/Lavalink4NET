﻿namespace Lavalink4NET.Player {
    /// <summary>
    /// Marker interface for track additional info
    /// </summary>
    public interface ILavalinkTrackAttribute { }
}