/*
 *  File:   PlayerFilterMap.cs
 *  Author: Angelo Breuer
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) Angelo Breuer 2022
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

namespace Lavalink4NET.Player;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lavalink4NET.Filters;
using Lavalink4NET.Payloads;
using Lavalink4NET.Payloads.Player;

public sealed class PlayerFilterMap : FilterMapBase
{
    private readonly LavalinkPlayer _player;
    private bool _changesToCommit;

    internal PlayerFilterMap(LavalinkPlayer player)
    {
        _player = player ?? throw new ArgumentNullException(nameof(player));
    }

    public async Task CommitAsync(bool force = false)
    {
        if (!_changesToCommit && !force)
        {
            return;
        }

        var payload = new PlayerFiltersPayload
        {
            GuildId = _player.GuildId,
            Filters = Filters,
        };

        await _player.LavalinkSocket
            .SendPayloadAsync(OpCode.PlayerFilters, payload)
            .ConfigureAwait(false);
    }

    public override IFilterOptions? this[string name]
    {
        get
        {
            return Filters.TryGetValue(name, out var options) ? options : null;
        }

        set
        {
            if (value is null)
            {
                if (Filters.Remove(name))
                {
                    _changesToCommit = true;
                }

                return;
            }

            Filters[name] = value!;
            _changesToCommit = true;
        }
    }
}

