﻿using System;

namespace Lavalink4NET.Events
{
    /// <summary>
    /// Event arguments for an event that indicates a node change action happened.
    /// </summary>
    public class SocketChangedEventArgs : EventArgs
    {
        public SocketChangedEventArgs(LavalinkSocket oldSocket, LavalinkSocket newSocket)
        {
            OldSocket = oldSocket;
            NewSocket = newSocket;
        }

        public LavalinkSocket OldSocket { get; set; }
        public LavalinkSocket NewSocket { get; set; }
    }
}